<?php

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
$logo = opt('logo');
$map = opt('map_image');

?>

<footer>
	<div class="footer-main">
		<a id="go-top">
			<?= svg_simple(ICONS.'to-top.svg'); ?>
			<span class="to-top-text">
				<?= lang_text(['he' => 'חזרה למעלה', 'en' => 'To top'], 'he'); ?>
			</span>
		</a>
		<?php if ($current_id !== $contact_id) : ?>
			<div class="foo-form-wrap">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="row justify-content-center align-items-center mb-3">
								<?php if ($f_title = opt('foo_form_title')) : ?>
									<div class="col-auto">
										<h2 class="foo-form-title"><?= $f_title; ?></h2>
									</div>
								<?php endif; ?>
								<?php if ($f_text = opt('foo_form_subtitle')) : ?>
									<div class="col-auto">
										<h3 class="foo-form-subtitle"><?= $f_text; ?></h3>
									</div>
								<?php endif; ?>
							</div>
							<div class="form-col">
								<?php getForm('44'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<div class="container position-relative">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row justify-content-between">
						<div class="col-xl-3 col-sm-6 col-12 foo-menu main-footer-menu">
							<h3 class="foo-title">
								<?= lang_text(['he' => 'תפריט ראשי //', 'en' => 'Main menu //'], 'he'); ?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-menu', '1', 'hop-hey'); ?>
							</div>
						</div>
						<div class="col-lg col-12 foo-menu links-footer-menu">
							<h3 class="foo-title">
								<?php $menu_title = opt('foo_menu_title');
								echo $menu_title ? $menu_title : lang_text(['he' => 'מאמרים רלוונטיים //', 'en' => 'Hop articles //'], 'he');?>
							</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-links-menu', '1', 'hop-hey three-columns'); ?>
							</div>
						</div>
						<div class="col-xl-3 col-sm-6 col-12 foo-menu contacts-footer-menu">
							<h3 class="foo-title">
								<?= lang_text(['he' => 'פרטי התקשרות // ', 'en' => 'Contact us //'], 'he'); ?>
							</h3>
							<div class="menu-border-top contact-menu-foo">
								<ul class="contact-list d-flex flex-column">
									<?php if ($tel) : ?>
										<li>
											<a href="tel:<?= $tel; ?>" class="contact-info-footer">
												<?= svg_simple(ICONS.'foo-tel.svg'); ?>
												<span><?= $tel; ?></span>
											</a>
										</li>
									<?php endif;
									if ($address) : ?>
										<li>
											<a href="https://waze.com/ul?q=<?= $address; ?>"
											   class="contact-info-footer" target="_blank">
												<?= svg_simple(ICONS.'foo-geo.svg'); ?>
												<span><?= $address; ?></span>
											</a>
										</li>
									<?php endif;
									if ($mail) : ?>
										<li>
											<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
												<?= svg_simple(ICONS.'foo-mail.svg'); ?>
												<span><?= $mail; ?></span>
											</a>
										</li>
									<?php endif; ?>
								</ul>
								<?php if ($logo) : ?>
									<a href="/" class="logo logo-footer align-self-end">
										<img src="<?= $logo['url'] ?>" alt="logo-euroseal">
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
