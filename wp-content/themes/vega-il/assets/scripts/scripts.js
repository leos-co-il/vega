(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	var width = $(window).width();
	if(width > 768){
		$('.menu-item-has-children').hover(function(){
			$(this).children('.sub-menu').fadeIn();
		}, function(){
			$(this).children('.sub-menu').fadeOut();
		});
	}
	$( document ).ready(function() {
		$('.counter-num').rCounter({
			duration: 30,
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.cat-item-img').hover(function (){
			$(this).children('.cat-item-info').children('.cat-item-info-part').slideFadeToggle();
		});
		$('.worker-img').click(function () {
			$(this).parent('.worker-item').children('.worker-item-overlay').addClass('show');
		});
		$('.close-worker').click(function () {
			$(this).parent('.worker-item-overlay').removeClass('show');
		});
		$('.pop-trigger').click(function () {
			$('.float-form').toggleClass('show-form');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.main-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: true,
		});
		$('.pro-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			dots: false,
			arrows: true,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button-about').click(function() {
			var id = $(this).data('video');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		//Socials
		$('.trigger-wrap').hover(function(){
			$(this).children('.all-socials').addClass('show-share');
		}, function(){
			$(this).children('.all-socials').removeClass('show-share');
		});
	});
	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading mx-2"><i class="fas fa-spinner fa-pulse"></i></div>');
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var type = $(this).data('type');
		var count = $(this).data('count');
		var page = $(this).data('page');
		var quantity = $('.more-card').length;

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				termID: termID,
				ids: ids,
				count: count,
				quantity : quantity,
				type: type,
				page: page,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
var v = document.getElementById('video-home');
var tr = document.getElementById('video-button');
if (tr && v) {
	tr.addEventListener(
		'play',
		function() {
			v.play();
		},
		false);

	tr.onclick = function() {
		if (v.paused) {
			v.play();
			tr.classList.add('hide');
		} else {
			v.pause();
			tr.classList.remove('hide');
		}
		return false;
	};
}
