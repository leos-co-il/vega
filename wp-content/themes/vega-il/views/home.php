<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<?php if ($fields['main_video']) : ?>
	<section class="video-home">
		<span class="video-home-overlay"></span>
		<video id="video-home">
			<source src="<?= $fields['main_video']['url']; ?>" type="video/mp4">
		</video>
		<span id="video-button" class="button-home">
			<?php if ($logo = opt('logo')) : ?>
				<a href="/" class="logo-home mb-3">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
			<?php endif; ?>
			<?= svg_simple(ICONS.'play.svg'); ?>
		</span>
	</section>
	<div class="home-video-bottom"></div>
<?php endif; ?>
<?php if ($fields['h_about_text']) : ?>
	<section class="home-about-block">
		<?php get_template_part('views/partials/content', 'text_centered',
			[
				'text' => $fields['h_about_text'],
			]);
		if (['h_about_link']) : ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<a href="<?= $fields['h_about_link']['url'];?>" class="block-link">
						<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
								? $fields['h_about_link']['title'] : lang_text(['he' => 'קרא עוד עלינו', 'en' => 'Read more about us'], 'he');
						?>
					</a>
				</div>
			</div>
		</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['h_counter']) : ?>
	<section class="counter-block" id="start-count">
		<div class="before-slider"></div>
		<div class="counter-wrapper blue-back">
			<div class="container">
				<div class="row justify-content-center align-items-start">
					<?php foreach ($fields['h_counter'] as $counter_item) : ?>
						<div class="col-md-4 col-12 mb-4 wow fadeInUp">
							<div class="counter-item">
								<h2 class="counter-number counter-num" data-from="0"
									data-to="<?= $counter_item['counter_number']; ?>" data-speed="1500">
									<?= $counter_item['counter_number']; ?>
								</h2>
								<h4 class="counter-title"><?= $counter_item['counter_title']; ?></h4>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['h_projects_text'] || $fields['h_projects']) : ?>
	<section class="home-projects-block">
		<?php if ($fields['h_projects_text']) {
			get_template_part('views/partials/content', 'text_centered',
					[
							'text' => $fields['h_projects_text'],
					]);
		}
		if ($fields['h_projects']) :
			$new_arr = $fields['h_projects'];
			if ($fields['h_projects_link']) {
			array_unshift($new_arr, $fields['h_projects_link']);
			}
			$chunks = array_chunk($new_arr, 4); ?>
			<div class="container my-5">
				<div class="row">
					<div class="col-12">
						<div class="row align-items-stretch">
							<?php foreach ($chunks as $x => $big_chunk) : ?>
								<div class="<?= ($x === 0 || $x === 2) ? 'col-xl-5 col-12 p-0' : 'col-xl-7 col-12 p-0'; ?> big-col">
									<?php $small_chunk = array_chunk($big_chunk, 2);
									if ($x === 1 || $x === 3) {
										$small_chunk = array_chunk($big_chunk, 3);
									} foreach ($small_chunk as $z => $item) : ?>
										<div class="gallery-col">
											<?php foreach ($item as $y => $pro) : ?>
												<?php if ($x == 0 && $y == 0 && $z == 0) : ?>
												<div class="gallery-item pro-item-home pro-item-link-wrap">
													<h3 class="other-pro-text">
														<?= lang_text(['he' => 'פרויקטים אחרונים', 'en' => 'Other projects'], 'he')?>
													</h3>
													<a class="pro-item-link" href="<?= $pro['url']; ?>">
														<?= (isset($pro['title']) && $pro['title'])
																? $pro['title'] : lang_text(['he' => 'לכל הפרויקטים', 'en' => 'To all projects'], 'he');
														?>
													</a>
												</div>
												<?php else: ?>
												<div class="gallery-item pro-item-home" <?php if (has_post_thumbnail($pro)) : ?>
													 style="background-image: url('<?php echo $image = postThumb($pro); ?>')" <?php endif; ?>>
													<div class="pro-item-inside">
														<a class="pro-item-overlay" href="<?= get_the_permalink($pro); ?>">
														<span class="pro-name-home">
															<?= $pro->post_title; ?>
														</span>
														</a>
													</div>
												</div>
											<?php endif; endforeach; ?>
										</div>
									<?php endforeach; ?>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['h_benefit_item']) : ?>
	<div class="benefits-block">
		<div class="before-slider"></div>
		<div class="benefits-wrapper blue-back">
			<div class="container">
				<div class="row justify-content-center align-items-start">
					<?php foreach ($fields['h_benefit_item'] as $x => $why_item) : ?>
						<div class="col-lg-4 col-sm-6 col-12 why-item-col wow fadeInUp" data-wow-delay="0.<?= $x + 1; ?>s">
							<div class="why-item">
								<div class="why-icon-wrap">
									<?php if ($why_item['ben_icon']) : ?>
										<img src="<?= $why_item['ben_icon']['url']; ?>">
									<?php endif; ?>
								</div>
								<h3 class="why-item-title">
									<?= $why_item['ben_title']; ?>
								</h3>
								<div class="why-item-text">
									<?= $why_item['ben_desc']; ?>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
		<div class="after-slider"></div>
	</div>
<?php endif;
if ($fields['h_posts_text'] || $fields['h_posts']) : ?>
	<section class="home-posts-block">
		<?php get_template_part('views/partials/content', 'text_centered',
				[
						'text' => $fields['h_posts_text'],
				]);
		if ($fields['h_posts']) : ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['h_posts'] as $post) {
						get_template_part('views/partials/card', 'post',
								[
										'post' => $post,
								]);
					} ?>
				</div>
			</div>
		<?php endif;
		if (['h_posts_link']) : ?>
			<div class="container mt-3">
				<div class="row justify-content-center">
					<div class="col-auto">
						<a href="<?= $fields['h_posts_link']['url'];?>" class="block-link">
							<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title'])
									? $fields['h_posts_link']['title'] : lang_text(['he' => 'לכל המאמרים', 'en' => 'To all articles'], 'he');
							?>
						</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['h_companies']) : ?>
	<section class="home-companies-block">
		<?php get_template_part('views/partials/content', 'text_centered',
				[
						'text' => $fields['h_companies_text'],
				]); ?>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-lg-12 col-md-11 col-12">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($fields['h_companies'] as $company) {
							get_template_part('views/partials/card', 'company',
									[
											'item' => $company,
									]);
						}; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
get_footer(); ?>
