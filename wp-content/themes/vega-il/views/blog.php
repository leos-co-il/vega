<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$type = $fields['choose_type'] ? $fields['choose_type'] : 'post';
$cat = ($type === 'project') ? 'project_cat' : 'category';
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => $type,
	'suppress_filters' => false
]);
$published_posts = '';
$count_posts = wp_count_posts($type);
if ( $count_posts ) {
	$published_posts = $count_posts->publish;
}
$cats = get_terms([
		'taxonomy' => $cat,
		'hide_empty' => false,
]);
?>

<article class="page-body p-block mb-5">
	<?php get_template_part('views/partials/content', 'top_page',
			[
				'img' => has_post_thumbnail() ? postThumb() : '',
				'title' => get_the_title(),
			]);
	if ($cats) : ?>
		<div class="container">
			<div class="row justify-content-center align-items-stretch mb-2">
				<?php foreach ($cats as $cat_item) : ?>
					<div class="col-auto d-flex justify-content-center align-items-center cat-link-col">
						<a href="<?= get_term_link($cat_item); ?>" class="cat-link">
							<?= $cat_item->name; ?>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="container">
		<div class="row justify-content-center mb-4">
			<div class="col-xl-8 col-lg-9 col-md-10 col-11">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
							[
									'post' => $post,
							]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts && $published_posts > 8) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="block-link more-link load-more-posts" data-type="<?= $type; ?>">
						<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more...'], 'he')?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($fields['single_slider_seo']) : ?>
	<div class="black-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
	]); ?>
	</div>
<?php endif;
get_footer(); ?>
