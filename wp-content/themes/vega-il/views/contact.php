<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$fax = opt('fax');
$address = opt('address');
$open_hours = opt('open_hours');
?>

<article class="page-body p-block mb-5 contact-page-main">
	<?php get_template_part('views/partials/content', 'top_page',
		[
			'img' => has_post_thumbnail() ? postThumb() : '',
			'title' => get_the_title(),
		]); ?>
	<div class="container">
		<div class="row justify-content-center mb-5">
			<div class="col-auto">
				<div class="row justify-content-center">
					<?php if ($tel) : ?>
						<div class="col-lg-3 col-sm-6 col-11 contact-item contact-item-link wow zoomIn"
							 data-wow-delay="0.2s">
							<a class="hidden-info" href="tel:<?= $tel; ?>"><?= $tel; ?></a>
							<a class="contact-icon-wrap" href="tel:<?= $tel; ?>">
								<img src="<?= ICONS ?>contact-tel.png">
							</a>
						</div>
					<?php endif;
					if ($fax) : ?>
						<div class="contact-item col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.4s">
							<h3 class="hidden-info"><?= $fax; ?></h3>
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-fax.png">
							</div>
						</div>
					<?php endif;
					if ($mail) : ?>
						<div class="contact-item col-lg-3 col-sm-6 col-11 contact-item-link wow zoomIn"
							 data-wow-delay="0.6s">
							<a class="hidden-info" href="mailto:<?= $mail; ?>"><?= $mail; ?></a>
							<a class="contact-icon-wrap" href="mailto:<?= $mail; ?>">
								<img src="<?= ICONS ?>contact-mail.png">
							</a>
						</div>
					<?php endif;
					if ($address) : ?>
						<a href="https://www.waze.com/ul?q=<?= $address; ?>"
						   class="contact-item-link contact-item contact-map col-lg-3 col-sm-6 col-11 wow zoomIn" data-wow-delay="0.8s">
							<?php if ($map = opt('map_image')) : ?>
							<div class="hidden-info-map">
								<img src="<?= $map['url']; ?>" alt="map">
							</div>
							<?php endif; ?>
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-geo.png">
							</div>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<?php getForm('48'); ?>
			</div>
		</div>
		<div class="row justify-content-center my-5">
			<div class="col-auto">
				<h2 class="base-title text-center mb-3">
					<?= lang_text(['he' => 'פרטי התקשרות ', 'en' => 'Contact us'], 'he'); ?>
				</h2>
				<ul class="contact-list contact-list-page d-flex flex-column">
					<?php if ($tel) : ?>
						<li>
							<a href="tel:<?= $tel; ?>" class="contact-info">
								<span><?= lang_text(['he' => 'טלפון:', 'en' => 'Phone:'], 'he').$tel; ?></span>
							</a>
						</li>
					<?php endif;
					if ($fax) : ?>
						<li>
							<span class="contact-info">
								<span><?= lang_text(['he' => 'פקס:', 'en' => 'Fax:'], 'he').$fax; ?></span>
							</span>
						</li>
					<?php endif;
					if ($mail) : ?>
						<li>
							<a href="mailto:<?= $mail; ?>" class="contact-info">
								<span><?= lang_text(['he' => 'מייל:', 'en' => 'Email:'], 'he').$mail; ?></span>
							</a>
						</li>
					<?php endif;
					if ($address) : ?>
						<li>
							<a href="https://waze.com/ul?q=<?= $address; ?>"
							   class="contact-info" target="_blank">
								<span><?= lang_text(['he' => 'כתובת:', 'en' => 'Address:'], 'he').$address; ?></span>
							</a>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>

