<?php
/*
Template Name: חברות קשורות
*/

get_header();
$fields = get_fields();
?>

<article class="page-body p-block mb-5">
	<?php get_template_part('views/partials/content', 'top_page',
			[
					'img' => has_post_thumbnail() ? postThumb() : '',
					'title' => get_the_title(),
			]); ?>
	<div class="container">
		<div class="row justify-content-center mb-4">
			<div class="col-xl-8 col-lg-9 col-md-10 col-11">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($fields['page_companies']) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($fields['page_companies'] as $x => $post) {
					if ($x < 4) { get_template_part('views/partials/card', 'company',
							[
									'item' => $post,
									'num' => $x,
							]);
				} } ?>
			</div>
		<?php endif;
		if (count($fields['page_companies']) > 4) : ?>
			<div class="row justify-content-center">
				<div class="col-auto mt-4">
					<div class="block-link more-link load-more-posts" data-type="company" data-page="<?= get_queried_object_id(); ?>">
						<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more...'], 'he')?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php if ($fields['single_slider_seo']) : ?>
	<div class="black-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>
