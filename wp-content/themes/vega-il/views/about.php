<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="page-body p-block mb-5">
	<?php get_template_part('views/partials/content', 'top_page',
			[
					'img' => has_post_thumbnail() ? postThumb() : '',
					'title' => get_the_title(),
			]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-12">
				<div class="row justify-content-center align-items-start">
					<div class="col-lg-10 col-12">
						<div class="base-output text-center">
							<?php the_content(); ?>
						</div>
					</div>
				</div>
				<?php if ($fields['about_video']) : ?>
					<div class="row justify-content-center">
						<div class="col-12 video-about-col">
							<div class="about-video">
								<span class="play-button-about" data-video="<?= getYoutubeId($fields['about_video']); ?>">
									<?= svg_simple(ICONS.'play.svg'); ?>
								</span>
								<div class="about-video-img" <?php if ($img = getYoutubeThumb($fields['about_video'])): ?>
									style="background-image: url('<?= $img; ?>')"<?php endif; ?>>
								</div>
								<div class="put-video-here"></div>
							</div>
						</div>
					</div>
				<?php endif;
				if ($fields['about_team_item']) : ?>
					<div class="row justify-content-center mt-4">
						<div class="col-auto">
							<h2 class="base-title text-center mb-3">
								<?= (isset($fields['about_team_title']) && $fields['about_team_title']) ? $fields['about_team_title'] :
								lang_text(['he' => 'הכירו את הצוות שלנו', 'en' => 'Our team'], 'he'); ?>
							</h2>
						</div>
					</div>
				 	<div class="row justify-content-lg-between justify-content-center align-items-stretch">
						<?php foreach ($fields['about_team_item'] as $x => $worker) : ?>
							<div class="col-lg-auto col-md-6 worker-col-item">
								<div class="worker-item">
									<?php if ($worker['worker_info']) : ?>
										<div class="worker-item-overlay">
											<span class="close-worker">
												<img src="<?= ICONS ?>close.png" alt="close">
											</span>
											<p class="worker-info-desc">
												<?= $worker['worker_info']; ?>
											</p>
										</div>
									<?php endif; ?>
									<div class="worker-img">
										<?php if ($worker['worker_img']) : ?>
											<img src="<?= $worker['worker_img']['url']; ?>" alt="worker-photo">
										<?php endif; ?>
									</div>
									<div class="worker-info-wrap mt-minus">
										<h3 class="worker-name"><?= $worker['worker_name']; ?></h3>
										<h3 class="worker-position"><?= $worker['worker_position']; ?></h3>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php if ($fields['single_slider_seo']) : ?>
	<div class="black-slider">
		<?php get_template_part('views/partials/content', 'slider', [
				'content' => $fields['single_slider_seo'],
				'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>
