<?php if (isset($args['text']) && $args['text']) : ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-9 col-md-10 col-11">
				<div class="base-output text-center mb-4">
					<?= $args['text']; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
