<?php if (isset($args['item']) && $args['item']) : ?>
	<div class="col-lg-3 col-sm-6 col-12 col-company more-card" data-id="<?= (isset($args['num']) && $args['num']) ? $args['num'] : ''; ?>">
		<a class="company-wrap" href="<?= isset($args['item']['company_link']) ? $args['item']['company_link'] : ''; ?>">
			<?php if (isset($args['item']['company_img']) && $args['item']['company_img']) : ?>
				<img src="<?= $args['item']['company_img']['url'];?>" alt="company-logo">
			<?php endif; ?>
		</a>
	</div>
<?php endif; ?>
