<div class="top-page-block mb-3">
	<div class="top-page-img" <?php if (isset($args['img']) && $args['img']) : ?>
		 style="background-image: url('<?= $args['img']; ?>')" <?php endif; ?>>
		<span class="video-home-overlay"></span>
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h1 class="top-title">
						<?= (isset($args['title']) && $args['title']) ? $args['title'] : ''; ?>
					</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="home-video-bottom"></div>
	<?php if ( function_exists('yoast_breadcrumb') ) : ?>
		<div class="container">
			<div class="row justify-content-center bread-row">
				<div class="col-12">
					<?php yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</div>
