<?php if (isset($args['post'])) : $link = get_the_permalink($post); ?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col post-col-base">
		<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-item-image" href=""
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-item-content">
				<h2 class="post-item-title"><?= $args['post']->post_title; ?></h2>
				<p class="post-item-text">
					<?= text_preview($args['post']->post_content, 10); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="post-link align-self-end">
				<?= lang_text(['he' => 'קרא עוד', 'en' => 'Read more'], 'he'); ?>
			</a>
		</div>
	</div>
<?php endif; ?>
