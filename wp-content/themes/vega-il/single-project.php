<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body p-block mb-5">
	<?php get_template_part('views/partials/content', 'top_page',
		[
			'img' => (isset($fields['top_img']) && $fields['top_img']) ? $fields['top_img']['url'] : '',
			'title' => get_the_title(),
		]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-10 col-12">
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['project_gallery']) : $chunks = array_chunk($fields['project_gallery'], 4); ?>
		<div class="container my-5">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="base-title mb-4">
						<?= $fields['project_gallery_title'] ? $sameTitle = $fields['project_gallery_title'] : lang_text(['he' => 'גלריה', 'en' => 'Gallery'], 'he'); ?>
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="row align-items-stretch">
						<?php foreach ($chunks as $x => $big_chunk) : ?>
							<div class="<?= ($x === 0 || $x === 2) ? 'col-xl-5 col-12 p-0' : 'col-xl-7 col-12 p-0'; ?> big-col">
								<?php $small_chunk = array_chunk($big_chunk, 2);
								if ($x === 1 || $x === 3) {
									$small_chunk = array_chunk($big_chunk, 3);
								} foreach ($small_chunk as $item) : ?>
									<div class="gallery-col">
										<?php foreach ($item as $img) : ?>
											<div class="gallery-item" style="background-image: url('<?= $img['url']; ?>')" >
												<a class="gallery-item-overlay" href="<?= $img['url']; ?>" data-lightbox="gallery">
													+
												</a>
											</div>
										<?php endforeach; ?>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'project_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 5,
		'post_type' => 'project',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'project_cat',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_projects']) {
	$samePosts = $fields['same_projects'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 5,
			'orderby' => 'rand',
			'post_type' => 'project',
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<div class="black-slider">
		<div class="before-slider">
		</div>
		<div class="same-projects-block arrows-slider pro-slider-wrap">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="same-projects-title">
							<?= $fields['same_projects_title'] ? $sameTitle = $fields['same_projects_title'] : lang_text(['he' => 'פרויקטים נוספים', 'en' => 'Same projects'], 'he'); ?>
						</h2>
					</div>
					<div class="col-11">
						<div class="pro-slider" dir="rtl">
							<?php foreach ($samePosts as $post) : ?>
								<div class="p-3">
									<a class="same-project-item" href="<?php the_permalink($post); ?>">
										<div class="same-project-img" <?php if (has_post_thumbnail($post)) : ?>
											style="background-image: url('<?= postThumb(); ?>')"
										<?php endif; ?>></div>
										<h3 class="same-project-title"><?= $post->post_title; ?></h3>
									</a>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif;
get_footer(); ?>
