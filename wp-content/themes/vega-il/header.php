<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_head(); ?>
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
	<script>
		const timerStart = Date.now();
	</script>
	<div class="debug bg-danger border">
		<p class="width">
			<span>Width:</span>
			<span class="val"></span>
		</p>
		<p class="height">
			<span>Height:</span>
			<span class="val"></span>
		</p>
		<p class="media-query">
			<span>Media Query:</span>
			<span class="val"></span>
		</p>
		<p class="zoom">
			<span>Zoom:</span>
			<span class="val"></span>
		</p>
		<p class="dom-ready">
			<span>DOM Ready:</span>
			<span class="val"></span>
		</p>
		<p class="load-time">
			<span>Loading Time:</span>
			<span class="val"></span>
		</p>
	</div>
<?php endif; ?>


<header>
	<div class="container">
		<div class="row align-items-center justify-content-between position-relative">
			<div class="drop-menu">
				<nav class="header-nav">
					<?php getMenu('dropdown-menu', '1'); ?>
				</nav>
			</div>
			<div class="col-auto col-none">
				<button class="hamburger hamburger--spin menu-trigger" type="button">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
				</button>
			</div>
			<div class="col">
				<nav id="MainNav" class="h-100">
					<div id="MobNavBtn">
						<span></span>
						<span></span>
						<span></span>
					</div>
					<?php getMenu('header-menu', '1', '', 'main_menu h-100'); ?>
				</nav>
			</div>
			<div class="col-xl-auto col-md-3 col-sm-5 header-logo-col">
				<div class="langs-wrap">
					<?php site_languages(); ?>
				</div>
				<?php if ($logo = opt('header_logo')) : ?>
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>
<div class="float-form">
	<div class="container">
		<div class="row justify-content-end align-items-end">
			<div class="col-xl-5 col-lg-7 col-12 col-xxl">
				<?php get_template_part('views/partials/repeat', 'form_item', [
						'title' => opt('pop_form_title'),
						'subtitle' => opt('pop_form_subtitle'),
						'id' => '42',
				]); ?>
			</div>
		</div>
	</div>
</div>
<div class="triggers-col">
	<?php if ($whatsapp = opt('whatsapp')) : ?>
		<a href=https://api.whatsapp.com/send?phone="<?= $whatsapp; ?>" class="trigger-item mb-3">
			<?= svg_simple(ICONS.'whatsapp.svg'); ?>
		</a>
	<?php endif; ?>
	<div class="pop-trigger-item pop-trigger">
		<span class="pop-trigger-text">
			<?= lang_text(['he' => 'צור קשר', 'en' => 'Contact us'], 'he'); ?>
		</span>
	</div>
</div>
