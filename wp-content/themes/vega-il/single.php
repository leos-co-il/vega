<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body p-block mb-5">
	<?php get_template_part('views/partials/content', 'top_page',
		[
			'img' => (isset($fields['top_img']) && $fields['top_img']) ? $fields['top_img']['url'] : '',
			'title' => get_the_title(),
		]); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-11 col-12">
				<div class="row justify-content-between">
					<div class="col-xl-6 col-lg-7 col-12">
						<div class="base-output post-output">
							<?php the_content(); ?>
						</div>
						<div class="socials-share">
					<span class="share-text">
						 <?= lang_text(['he' => 'שתף', 'en' => 'Share'], 'he'); ?>
					</span>
							<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
							   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
								<img src="<?= ICONS ?>share-facebook.png">
							</a>
							<!--	WHATSAPP-->
							<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
							   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
								<img src="<?= ICONS ?>share-whatsapp.png">
							</a>
							<!--	MAIL-->
							<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
							   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
								<img src="<?= ICONS ?>share-mail.png">
							</a>
						</div>
					</div>
					<div class="col-xxl-5 col-xl-6 col-lg-5 col-12 page-form-col-post">
						<?php if (has_post_thumbnail()) : ?>
							<img src="<?= postThumb(); ?>" alt="post-image" class="post-page-img w-100">
						<?php endif;
						get_template_part('views/partials/repeat', 'form_item', [
							'title' => opt('post_form_title'),
							'subtitle' => opt('post_form_subtitle'),
							'id' => '46',
						]); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php if ($fields['single_slider_seo']) : ?>
	<div class="black-slider">
		<?php get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
		]); ?>
	</div>
<?php endif;
get_footer(); ?>
