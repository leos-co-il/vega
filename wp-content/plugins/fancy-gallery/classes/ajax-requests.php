<?php Namespace WordPress\Plugin\GalleryManager;

abstract class AJAX_Requests {

  static function init(){
    static::registerAJAXHook('get_gallery', 'getGallery');
  }

  static function registerAJAXHook($action, $method){
    add_Action("wp_ajax_{$action}", [static::class, $method]);
    add_Action("wp_ajax_nopriv_{$action}", [static::class, $method]);
  }

  static function sendResponse($response){
    Header('Content-Type: application/json');
    echo JSON_Encode($response);
    exit;
  }

  static function getGallery(){
    $gallery_id = trim($_REQUEST['gallery_id']);
    $gallery = new Gallery($gallery_id);
    $arr_images = $gallery->getImages();
    $arr_images = Array_Values($arr_images);

  	if (empty($arr_images)) return False;

    foreach ($arr_images as &$attachment_ptr){
      $img_obj = (object) [
        'title' => isSet($attachment_ptr->post_title) ? $attachment_ptr->post_title : False,
        'description' => isSet($attachment_ptr->post_content) ? $attachment_ptr->post_content : False,
        'href' => $attachment_ptr->url,
        'thumbnail' => isSet($attachment_ptr->thumbnail->url) ? $attachment_ptr->thumbnail->url : False
      ];

      # Overwrite image item at attachment_ptr with new image object
      $attachment_ptr = apply_Filters('gallery_manager_json_image', $img_obj, $attachment_ptr, $gallery);
    }

    # return the images
    static::sendResponse($arr_images);
  }

}

AJAX_Requests::init();
